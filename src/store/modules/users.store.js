import usersApi from "../../api/users.api";

const state = {
    users: []
};
const getters = {
    getUsers: state => state.users
};
const mutations = {

};
const actions = {
    createUser({}, data) {
        dashboardApi.create(data,
            (result) => {
                console.log("Exito!");
            }, (error) => {
                console.log("Fail!");
            });
    }
};

export default {
    namespaced: true,
    state: {
        ...state
    },
    getters: {
        ...getters
    },
    mutations,
    actions
};