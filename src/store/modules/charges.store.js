import paymentsApi from "@/api/payments.api";
import typesApi from "@/api/tipoDomiciliado.api";

const state = {
  charges: []
};
const getters = {
  getCharges: state => state.charges
};
const mutations = {
  SET_CHARGE(state, charge) {
    state.charges.push(charge);
  },
  INIT_CHARGE(state, charges) {
    let newArr = [];
    charges.forEach(element => {
      newArr.push(element);
    });
    state.charges = newArr;
  }
};
const actions = {
  create({ commit }, data) {
    paymentsApi.create(
      data,
      result => {
        commit("SET_CHARGE", result.data);
      },
      error => {
        return error;
      }
    );
  },
  validate({commit, state}, data){
    paymentsApi.patchPayment(data,
      result => {
        let newArr = state.charges;
        for(let charge in newArr){
          if (newArr[charge]._id == result.data._id) {
            newArr[charge] = result.data;
          }
        }
        commit("INIT_CHARGE", newArr);
      },
      error => {
        return error;
      })
  },
  get({ commit }) {
    paymentsApi.get(
      result => {
        commit("INIT_CHARGE", result.data);
      },
      error => {
        console.log(error);
      }
    );
  },
  getTypes({ commit }) {
    typesApi.get(
      result => {
        console.log(result.data);
    },
    error => {

    })
  }
};

export default {
  namespaced: true,
  state: {
    ...state
  },
  getters: {
    ...getters
  },
  mutations,
  actions
};
