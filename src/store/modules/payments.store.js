import paymentsApi from "@/api/payments.api";

const state = {
  payments: [],
  oldPayments: []
};
const getters = {
  getPayments: state => state.payments,
  getOldPayments: state => state.oldPayments
};
const mutations = {
  SET_PAYMENTS(state, payments) {
    state.payments = payments;
  },
  SET_PAST_PAYMENTS(state, payments) {
    state.oldPayments = payments;
  }
};
const actions = {
  getByUser({ commit }, state) {
    paymentsApi.getByUserStatus(
      {
        user_id: "5da8ff4824ef634e33e18eec",
        state: state
      },
      result => {
        if (state == "Por pagar") {
          commit("SET_PAYMENTS", result.data);
        }
        if (state == "pagado") {
          commit("SET_PAST_PAYMENTS", result.data);
        }
      },
      error => {
        return error;
      }
    );
  },
  addComprobante({ commit, state }, data) {
    paymentsApi.patchPayment(
      data,
      () => {
        const newPayments = state.payments.filter(
          payment => payment._id != data.idPayment
        );
        commit("SET_PAYMENTS", newPayments);
      },
      error => {
        return error;
      }
    );
  }
};

export default {
  namespaced: true,
  state: {
    ...state
  },
  getters: {
    ...getters
  },
  mutations,
  actions
};
