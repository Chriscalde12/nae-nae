import activityApi from "@/api/activities.api";

const state = {
  activities: []
};
const getters = {
  getActivities: state => state.activities
};
const mutations = {
  SET_ACTIVITIES(state, activities) {
    state.activities.push(activities);
  },
  INIT_ACTIVITIES(state, activities) {
    state.activities = activities;
  }
};
const actions = {
  create({ commit }, data) {
    activityApi.create(
      data,
      result => {
        console.log(result.data);
        commit("SET_ACTIVITIES", result.data);
      },
      error => {
        return error;
      }
    );
  },
  get({ commit }) {
    activityApi.get(
      result => {
        commit("INIT_ACTIVITIES", result.data);
      },
      error => {
        console.log(error);
      }
    );
  },
  update({ commit, state }, data) {
    activityApi.patch(data,
      result => {
        state.activities.forEach((act, index, array) => {
          if (act == result.data._id) {
            array[index] = result.data;
          }
        });
        commit("INIT_ACTIVITIES", state.activities);
      },
      error => {
        console.log(result.data);
      }
    );
  },
  delete({ commit, state }, actividad_id) {
    activityApi.delete(
      actividad_id,
      result => {
        const activities = state.activities.filter(x => {
          return x._id != actividad_id;
        });
        commit("INIT_ACTIVITIES", activities);
      },
      error => {
        console.log(error);
      });
  }
};

export default {
  namespaced: true,
  state: {
    ...state
  },
  getters: {
    ...getters
  },
  mutations,
  actions
};
