import Vue from "vue";
import Vuex from "vuex";

import actions from "@/store/actions";
import store from "@/store/store";
import mutations from "@/store/mutations";

import dashboard from "@/store/modules/dashboard.store";
import charges from "@/store/modules/charges.store";
import payments from "@/store/modules/payments.store";
import activities from "@/store/modules/activities.store";
import users from "@/store/modules/users.store";


Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        dashboard: dashboard,
        charges: charges,
        payments: payments,
        activities: activities,
        users: users
    },
    store: store,
    mutations: mutations,
    actions: actions
});