import config from "@/../config";
import axios from "axios";

export default {
  create(params = {}, onSuccess, onError) {
    return axios
      .post(`${config.host}/pagos`, params)
      .then(onSuccess)
      .catch(onError);
  },
  get(onSuccess, onError) {
    return axios
      .get(`${config.host}/pagos`)
      .then(onSuccess)
      .catch(onError);
  },
  getByUserStatus(params = {}, onSuccess, onError) {
    return axios
      .get(`${config.host}/pagos/usuario/${params.user_id}`, {
        params: {
          state: params.state
        }
      })
      .then(onSuccess)
      .catch(onError);
  },
  patchPayment(params = {}, onSuccess, onError) {
    return axios
      .patch(`${config.host}/pagos/${params.idPayment}/${params.idUser}`, params.body)
      .then(onSuccess)
      .catch(onError);
  }
};
