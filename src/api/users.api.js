import config from "@/../config";
import axios from "axios";

export default {
    create(params = {}, onSuccess, onError) {
        return axios
            .post(`${config.host}/users/`, params)
            .then(onSuccess)
            .catch(onError);
    },
    get(onSuccess, onError) {
        return axios
            .get(`${config.host}/users`)
            .then(onSuccess)
            .catch(onError);
    },
    patch(params = {}, onSuccess, onError) {
        return axios
            .patch(`${config.host}/users/${params._id}`, params)
            .then(onSuccess)
            .catch(onError);
    },
    delete(actividad_id, onSuccess, onError) {
        return axios
            .delete(`${config.host}/users/${actividad_id}`)
            .then(onSuccess)
            .catch(onError);
    }
};