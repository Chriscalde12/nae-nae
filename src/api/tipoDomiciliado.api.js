import config from "@/../config";
import axios from "axios";

export default {
  get(onSuccess, onError) {
    return axios
      .get(`${config.host}/tipo_domiciliado`)
      .then(onSuccess)
      .catch(onError);
  }
};
