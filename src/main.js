import Vue from "vue";
import ShardsVue from "shards-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import BootstrapVue from "bootstrap-vue";
import moment from "moment";

import "bootstrap/dist/css/bootstrap.css";
import "@/scss/shards-dashboards.scss";
import "@/assets/scss/date-range.scss";
import "tachyons";
import "vue2-timepicker/dist/VueTimepicker.css";

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.prototype.$eventHub = new Vue();
ShardsVue.install(Vue);

Vue.filter("date", function(value) {
  if (value) {
    return moment(String(value)).format("DD/MM/YYYY HH:mm");
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
