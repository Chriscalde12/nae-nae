import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Dashboard from "@/views/Dashboard";
import Activities from "@/views/Activities";
import ChargesDom from "@/views/ChargesDom";
import Charges from "@/views/Charges";
import Payments from "@/views/Payments";
import Users from "@/views/User";


Vue.use(Router);

export default new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [{
            path: "/",
            name: "home",
            component: Home,
            children: [{
                    // UserProfile will be rendered inside User's <router-view>
                    // when /user/:id/profile is matched
                    path: "home",
                    component: Dashboard
                },
                {
                    path: "activities",
                    component: Activities
                },
                {
                    path: "chargesdom",
                    component: ChargesDom
                },
                {
                    path: "charges",
                    component: Charges
                },
                {
                    path: "payments",
                    component: Payments
                },
                {
                    path: "users",
                    component: Users
                }
            ]
        },
        {
            path: "/dashboard",
            name: "dashboard",
            component: Dashboard
        }
    ]
});